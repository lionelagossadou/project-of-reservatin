import {
  createStore
} from 'vuex'
const axios = require('axios');


const instance = axios.create({
  baseURL: 'http://localhost:3000/'
});

// Creation d'une nouvelle instance du store.
const store = createStore({
  state: {
    users:[]
  },

  actions: {
    createAccount: ({
      commit
    }, userInfos) => {
      commit;
      instance.post('/internship', userInfos)
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  }
})
export default store;