import {createWebHistory, createRouter} from "vue-router";
import Register from "@/components/Register";
import List from "@/components/List";
import Login from "@/components/Login";
import Inscription from "@/components/Inscription";


const routes = [
    {
        path:'/Inscription',
        name:"Inscription",
        component:Inscription,
    },
    {
        path: '/register/:email',
        name: "Register",
        component: Register,
    },
    {
        path: '/list/:email',
        name: "List",
        component: List,

    },

    {
        path: '/:email',
        name: "Login",
        component: Login,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;